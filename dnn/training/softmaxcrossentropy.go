package training

import (
	"math"

	"gitlab.com/akita/dnn/tensor"
)

type SoftmaxCrossEntropy struct{}

func (s SoftmaxCrossEntropy) Loss(
	output tensor.Tensor,
	label []int,
) (
	loss float64,
	derivative *tensor.SimpleTensor,
) {
	outputVector := output.Vector()
	loss = 0
	for i := 0; i < len(label); i++ {
		start := i * output.Size()[1]
		end := start + output.Size()[1]
		outputSlice := outputVector[start:end]
		denominator := s.softmaxDenominator(outputSlice)
		loss += -1 * math.Log(math.Exp(outputSlice[label[i]])/denominator)
	}
	loss /= float64(output.Size()[0])

	derivativeVector := make([]float64, len(outputVector))
	for i := 0; i < len(label); i++ {
		for j := 0; j < output.Size()[1]; j++ {
			index := i*output.Size()[1] + j
			if label[i] == j {
				derivativeVector[index] = outputVector[index] - 1
			} else {
				derivativeVector[index] = outputVector[index]
			}
		}
	}

	derivative = &tensor.SimpleTensor{}
	derivative.Init(derivativeVector, output.Size())

	return loss, derivative
}

func (s SoftmaxCrossEntropy) softmaxDenominator(array []float64) float64 {
	sum := 0.0
	for _, val := range array {
		sum += math.Exp(val)
	}
	return sum
}
