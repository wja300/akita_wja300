package optimization

import (
	"gitlab.com/akita/dnn/layers"
)

// An Alg can uses the gradient to update model parameters.
type Alg interface {
	UpdateParameters(layer layers.Layer)
}
