package optimization

import (
	"github.com/golang/mock/gomock"
	. "github.com/onsi/ginkgo"
)

var _ = Describe("SGD", func() {
	var (
		mockCtrl          *gomock.Controller
		layer             *MockLayer
		params, gradients *MockVector
		sgd               *SGD
	)

	BeforeEach(func() {
		mockCtrl = gomock.NewController(GinkgoT())
		layer = NewMockLayer(mockCtrl)
		params = NewMockVector(mockCtrl)
		gradients = NewMockVector(mockCtrl)
		sgd = NewSGD(0.3)

		layer.EXPECT().Parameters().Return(params).AnyTimes()
		layer.EXPECT().Gradients().Return(gradients).AnyTimes()
	})

	AfterEach(func() {
		mockCtrl.Finish()
	})

	It("should run SGD", func() {
		params.EXPECT().ScaleAdd(1.0, -0.3, gradients)
		sgd.UpdateParameters(layer)
	})
})
