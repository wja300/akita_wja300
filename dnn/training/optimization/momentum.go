package optimization

import (
	"gitlab.com/akita/dnn/layers"
	"gitlab.com/akita/dnn/tensor"
)

// Momentum runs a exponentially weighted averaging over gradients.
type Momentum struct {
	LearningRate float64
	SmoothFactor float64

	history map[layers.Layer]tensor.Vector
}

func NewMomentum(learningRate float64, smoothFactor float64) *Momentum {
	return &Momentum{
		LearningRate: learningRate,
		SmoothFactor: smoothFactor,
		history:      make(map[layers.Layer]tensor.Vector),
	}
}

func (m *Momentum) UpdateParameters(layer layers.Layer) {
	params := layer.Parameters()
	gradients := layer.Gradients()

	if params == nil || gradients == nil {
		return
	}

	velocity, found := m.history[layer]
	if !found {
		velocity = gradients.Clone()
		m.history[layer] = velocity
	} else {
		velocity.ScaleAdd(m.SmoothFactor, 1-m.SmoothFactor, gradients)
	}

	params.ScaleAdd(1, -1.0*m.LearningRate, velocity)
}
