package mnist

import (
	"gitlab.com/akita/dnn/tensor"
)

// A DataSource provides convenient solution to feed data to neural networks.
type DataSource struct {
	dataSet   *DataSet
	imageSize int
	currPtr   int
	allData   []float64
	allLabel  []int
}

func NewTrainingDataSource() *DataSource {
	ds := &DataSource{
		imageSize: 784,
	}

	ds.dataSet = new(DataSet)
	ds.dataSet.OpenTrainingFile()

	ds.loadAllData()

	return ds
}

func NewTestDataSource() *DataSource {
	ds := &DataSource{
		imageSize: 784,
	}

	ds.dataSet = new(DataSet)
	ds.dataSet.OpenTestFile()

	ds.loadAllData()

	return ds
}

func (ds *DataSource) loadAllData() {
	for ds.dataSet.HasNext() {
		d, l := ds.dataSet.Next()
		normalizedD := make([]float64, len(d))
		for i := 0; i < len(d); i++ {
			normalizedD[i] = float64(d[i]) / 255
		}
		ds.allData = append(ds.allData, normalizedD...)
		ds.allLabel = append(ds.allLabel, int(l))
	}
}

func (ds *DataSource) NextBatch(batchSize int) (data tensor.Tensor, label []int) {
	start := ds.currPtr
	end := start + batchSize

	if end > len(ds.allLabel) {
		end = len(ds.allLabel)
	}

	rawData := ds.allData[start*ds.imageSize : end*ds.imageSize]
	data = &tensor.SimpleTensor{}
	data.Init(rawData, []int{end - start, ds.imageSize})

	label = ds.allLabel[start:end]

	ds.currPtr = end

	return data, label
}

func (ds *DataSource) Rewind() {
	ds.currPtr = 0
}
