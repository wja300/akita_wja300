package tensor

import "math"

type Vector interface {
	Raw() []float64

	// Clone creates a new vector with the same data
	Clone() Vector

	// Scale multiplies every element by alpha.
	Scale(alpha float64)

	// Add adds another vector element wise.
	Add(b Vector)

	// AddScalar adds the same alpha to all the element.
	AddScalar(alpha float64)

	// ScaleAdd calculates A = alpha * A + beta * B.
	ScaleAdd(alpha, beta float64, b Vector)

	// MulElemWide multiplies each element in A with each element in B.
	MulElemWise(b Vector)

	// DivElemWise divides each element in A by each element in B.
	DivElemWise(b Vector)

	// PowerScalar calculates the power of each element.
	PowerScalar(alpha float64)
}

type CPUVector []float64

func NewCPUVector(size int) CPUVector {
	return make([]float64, size)
}

func (v CPUVector) Raw() []float64 {
	return v
}

func (v CPUVector) Clone() Vector {
	newVec := NewCPUVector(len(v))
	copy(newVec, v)
	return newVec
}

func (v CPUVector) Scale(alpha float64) {
	for i := range v {
		v[i] *= alpha
	}
}

func (v CPUVector) Add(b Vector) {
	bRaw := b.Raw()
	for i := range v {
		v[i] += bRaw[i]
	}
}

func (v CPUVector) AddScalar(alpha float64) {
	for i := range v {
		v[i] += alpha
	}
}

func (v CPUVector) ScaleAdd(alpha, beta float64, b Vector) {
	bRaw := b.Raw()
	for i := range v {
		v[i] = alpha*v[i] + beta*bRaw[i]
	}
}

func (v CPUVector) MulElemWise(b Vector) {
	bRaw := b.Raw()
	for i := range v {
		v[i] *= bRaw[i]
	}
}

func (v CPUVector) DivElemWise(b Vector) {
	bRaw := b.Raw()
	for i := range v {
		v[i] /= bRaw[i]
	}
}

func (v CPUVector) PowerScalar(alpha float64) {
	for i := range v {
		v[i] = math.Pow(v[i], alpha)
	}
}
