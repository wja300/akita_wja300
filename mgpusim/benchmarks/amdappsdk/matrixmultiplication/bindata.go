// Code generated by go-bindata.
// sources:
// kernels.hsaco
// DO NOT EDIT!

package matrixmultiplication

import (
	"bytes"
	"compress/gzip"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
	"time"
)

func bindataRead(data []byte, name string) ([]byte, error) {
	gz, err := gzip.NewReader(bytes.NewBuffer(data))
	if err != nil {
		return nil, fmt.Errorf("Read %q: %v", name, err)
	}

	var buf bytes.Buffer
	_, err = io.Copy(&buf, gz)
	clErr := gz.Close()

	if err != nil {
		return nil, fmt.Errorf("Read %q: %v", name, err)
	}
	if clErr != nil {
		return nil, err
	}

	return buf.Bytes(), nil
}

type asset struct {
	bytes []byte
	info  os.FileInfo
}

type bindataFileInfo struct {
	name    string
	size    int64
	mode    os.FileMode
	modTime time.Time
}

func (fi bindataFileInfo) Name() string {
	return fi.name
}
func (fi bindataFileInfo) Size() int64 {
	return fi.size
}
func (fi bindataFileInfo) Mode() os.FileMode {
	return fi.mode
}
func (fi bindataFileInfo) ModTime() time.Time {
	return fi.modTime
}
func (fi bindataFileInfo) IsDir() bool {
	return false
}
func (fi bindataFileInfo) Sys() interface{} {
	return nil
}

var _kernelsHsaco = []byte("\x1f\x8b\x08\x00\x00\x00\x00\x00\x00\xff\xec\x5a\x5f\x50\x1b\xd7\xd5\x3f\x7b\x77\xb5\x5a\x5f\x04\x48\x18\xcb\x80\xfd\x11\x85\x0f\x03\xe6\x5b\x14\x7c\x2d\x84\x2c\x63\x23\x83\x0c\x36\x91\x09\x36\x31\x0e\x18\x8c\xd7\xd2\x22\x14\xeb\x0f\x11\xc2\x9f\xc9\xc4\x37\x12\x16\x84\x3a\x44\xa6\x34\xd3\xf1\xa4\x99\xd2\xc7\x74\xa6\x0f\xed\x53\xde\x0c\x99\xf6\x21\xd3\x27\xdb\x4f\x79\xf0\x43\x5f\x32\xd3\xe9\x74\x3a\x7d\x69\x9f\x3a\xa5\x73\xa5\x5d\x21\x29\x96\xe3\xa4\xe9\x64\xa6\xe1\xcc\xec\xfe\xf6\x9e\xff\xe7\xde\x63\xad\x2f\x77\xdf\x3d\xeb\x1b\x40\x1c\xe7\x01\x8d\x78\xf8\x03\x70\xec\xc1\x9c\x1b\xeb\x82\xbf\xbe\x9c\xc3\xf6\x2c\xcf\x05\x12\x78\xc0\x04\x18\x44\x00\x10\x0a\xf4\x4a\x71\x9b\x2b\x46\x49\xe3\x73\x9a\x5d\x39\xf2\x56\x14\xa3\x9e\x0f\xb3\x33\x14\x8c\x4b\xf1\x81\xb1\x18\x0b\xed\x58\xae\x60\xd3\xf8\x25\x38\x07\xc5\xa8\xdb\xa1\x6f\x68\xa7\xd7\x77\xe9\xcb\x44\x40\x78\x01\xbb\xc2\xfc\x18\x5d\xfc\x32\x11\x10\x9f\x33\x2f\xe5\x48\xd0\xae\x77\x50\x6e\x5c\x8a\x9f\x4b\xc5\xf8\xbc\xb9\xd7\xf3\x69\x60\x95\x97\xcc\x2b\x57\x70\x89\xf9\xd8\xc9\xec\xf8\xd3\xb6\xb7\x56\x91\x36\x67\x5c\xa9\xc3\x48\x24\xf2\xaa\x1a\x8f\xaa\xe1\xe9\x70\xcc\xaf\x84\xf5\x1c\x24\x4d\xf7\xcc\x05\x6f\xde\x8e\xf1\x0f\x65\x7b\x31\xc7\x17\xc0\x98\x9f\x57\x9d\x77\xe6\x82\x77\x70\xe4\x72\x4e\x97\xb5\x66\x85\xc6\x57\x22\x81\xa0\x3f\xda\xa1\x44\x02\xec\x9a\x9d\x57\x18\x04\x43\x6f\xfb\xc3\x1d\xc1\x99\xdb\xae\xce\xe3\x9a\xff\x5e\x09\x00\x6b\x36\x1d\x1d\x1d\x78\x4c\x8d\xcf\x87\x62\x51\xb7\x4d\xa7\xab\xb6\x63\xb2\xad\xd3\x36\x85\x73\x59\xcf\xef\x4a\xb0\xcd\xd6\x61\x1b\x56\x22\xea\x2e\xcb\x66\xb3\x95\xd4\x87\x19\x6f\x74\x31\x72\x23\x16\x2e\x50\x6d\x2d\xd1\xf2\xdc\x0c\xb4\x66\x35\x7d\x4a\x34\xb8\xa0\x04\x77\x5d\xbe\x36\xa7\x46\xfb\x7d\xb6\xfe\x22\x69\x3e\xc9\x6c\x72\xc4\x36\x95\x95\x9e\x89\x07\xe7\x8b\x52\xc1\x39\x78\x56\x92\x4a\x22\x1e\xba\x7d\x06\xeb\xe3\xd7\x17\xe7\xd4\x22\xa5\xd6\x99\x70\x4c\x49\x38\xda\x5b\xf3\x2a\xa3\xa1\xb7\x8b\x7d\xb8\xf2\xa2\x33\xe1\x50\x30\xea\x7e\xa6\x68\x4c\x09\x2f\xa8\xaf\x86\xa2\x01\x5d\x3c\x18\x8e\xdd\x50\xc2\x7d\x0b\x33\x33\x6a\xbc\x58\x8b\xe5\xa0\x6b\x0d\x1c\x27\xbb\xde\x03\x81\xf8\xe8\x9c\xe2\x57\x2f\x2e\x28\x61\x77\xde\xc5\xae\xdc\xef\xd7\x25\x39\xf2\xaa\x33\xca\x42\x38\xf1\x75\xd5\xf7\xfd\xa0\xab\xef\xff\x81\x56\xff\xff\xa1\x40\x62\xf6\x39\x8d\x1f\x8a\x26\xca\x97\xed\x28\x5f\xb6\xa3\x7c\xd9\x7d\x8b\x59\x56\xf9\x8a\xcf\x17\x56\xfc\x8d\x2b\xba\x11\x8e\xf9\x6f\xfe\x7b\xff\x94\xbf\x5d\x5d\xde\xc5\xa8\x12\x09\xf9\x47\x67\x95\xb8\x1a\x18\x89\x85\xa2\x89\x17\x5d\xd6\x9c\xb2\xba\x1b\xec\x98\xb3\xfc\x92\xfb\xf2\x3f\xa3\x2f\x34\x3f\xdf\x51\xaf\x9e\x0b\x05\x02\x6a\x34\xd7\x6e\xaf\xcd\xcc\xcc\xab\x89\x37\x9e\xb3\x80\x4e\xc7\x7f\x3e\xfe\xf8\xf7\x1c\x7f\xe2\xeb\xe3\xf7\xc7\x02\xea\x48\x3c\x36\x97\x7f\x0d\x69\x26\xec\x55\xa7\xc4\x83\xa3\x6a\x30\xa2\x46\x13\xb9\x0c\xf3\x29\x0f\xc6\x63\x0b\x73\x9a\x68\x20\x74\x5b\x0d\xe4\xe4\x9d\x9a\x78\x24\x1e\xba\xa5\x24\xd4\xf2\x0a\xc5\xce\xb5\x1a\xf5\xca\xae\x28\xb7\xd4\x99\x78\x4c\x0f\x6a\xdb\x0d\x3b\xbc\x10\x19\x1d\x1c\xb9\xb4\xfb\xc6\x3c\xde\xb9\x2b\x19\x2b\x92\x74\xeb\x36\x17\x94\xdb\x03\x61\x25\x71\x25\x16\xbf\x99\xcb\x3a\xeb\x94\x74\x39\xb1\xdd\x6e\xc7\xdf\xe2\x7f\x6b\x7b\xb4\x47\x7b\xb4\x47\xdf\x3f\xed\xee\xe7\xa4\xdc\xee\xea\x2b\x1b\xb7\x62\x7a\x02\xbf\x82\x47\xfb\xd8\x7e\xab\xf8\x77\xcf\x53\xf0\xdc\x08\x43\x45\x32\x41\x10\xc4\x9d\x9d\x9d\x9d\xef\x32\xef\xef\x8a\x10\xa0\x6d\xb6\x1f\x5c\x42\x68\xdb\xc4\x66\x40\x40\xdb\x75\x00\xb0\x03\xef\x3f\x64\x19\xf3\x3c\xde\x66\x5b\xf6\xbb\x20\x6e\xb3\xad\xf0\xbb\xb0\xb6\x05\x15\xd5\x2b\x52\xb5\x65\x03\x5b\x92\x29\xa8\x5e\xbd\x2b\xac\x40\xd2\x90\xe4\x96\x38\x24\x52\x40\x02\x05\x38\xf7\x14\x01\xf0\x92\x49\xa0\xe6\x4d\x58\x87\x9f\xc3\x7d\x33\x40\x0a\x0d\x0a\x14\x96\xcc\xeb\xb0\x69\x58\x37\x1b\x20\xb5\x23\x08\x18\x20\xf9\x1a\xaa\xc6\x14\x0c\x95\xab\x16\x90\x88\x49\x12\x88\x08\xcb\x4f\x0c\x16\x00\x23\xac\x64\x11\x2f\x09\x99\xa4\xc9\xec\x00\xf8\xfd\xa3\x64\x15\x62\xfb\xee\xc7\xa2\x85\xe1\xca\x13\xb1\x02\x81\x58\x51\x45\xc4\x0a\xec\x34\x00\x3c\x36\x54\x23\x30\x30\xbb\x43\x6c\x7f\x9e\xf3\x53\x69\xfa\xf8\x8e\x11\xe0\x71\x72\xe9\x97\x5c\xa5\xc9\xe4\x90\x4c\x3f\xbd\x23\x6a\x63\xb1\xda\xd4\x92\xc2\x66\x92\x34\x7d\x78\x47\xaa\x30\xc1\x67\x18\x93\x64\xd5\x87\x77\xc4\x0a\x0c\x06\x8c\xdb\x0d\xd8\xec\x4c\x99\x57\xef\xd0\x37\xff\xb4\x92\x84\x0f\x1e\xfe\x0f\xf7\xde\xd6\x5d\x41\xdc\x66\x73\xc7\x1b\xc4\xed\x5c\xd7\xa0\xed\xc3\xd9\x39\xc4\xcd\x2c\xa6\x19\xb3\x1c\x7e\xfb\x58\xc2\x48\x48\x63\xdc\x9c\x9d\x33\x7c\x98\x48\x69\x2e\xc3\x1d\x6e\x24\x5c\xa3\x8d\x70\xb6\x26\x82\x25\x6e\x83\x5b\x32\x66\x70\x8a\xcb\x70\x12\x26\x22\xd7\x40\x30\x00\xc1\x77\xb9\x0d\x0b\x00\xe1\x24\x89\x08\x9b\x86\x75\xcb\x67\x96\x54\x12\xc9\xd4\x24\x39\x89\x84\x24\xb3\x90\x7e\x90\xb1\xe0\x54\xaa\x5a\x58\x7e\xc8\x19\xee\xa7\x57\x24\xa9\xd9\xca\xdb\xa9\x95\xef\xa4\x56\x9e\x50\x2b\xdf\x4c\xad\x7c\x0d\xad\x4e\x73\x19\x90\x5c\xc4\x84\x1d\xc4\xca\x4b\xb4\x7a\x19\x32\x26\x00\x22\x2d\x89\x19\x2b\xfc\xee\x91\x04\x1c\x24\xb9\xe5\x54\x12\xd6\x52\x8f\x76\xee\x7f\x6a\xde\xb4\xac\x23\xde\x4d\x0f\xf2\x2d\xb4\x8e\x6f\xa3\xf5\x7c\x3b\x3d\xc8\xd7\xd2\x3a\xde\x4a\xeb\xf9\x3a\x7a\x90\xc7\xb4\x8e\x37\xd1\x7a\xbe\x8a\x56\x4a\xb0\x01\x0d\x5e\xb2\xe9\x1d\x68\x6a\x84\xfb\x4f\xd2\x5e\x04\xfb\x91\x87\xd6\xf4\x0c\x10\x5b\xef\x39\x17\xc0\xe4\xd3\xff\x05\x68\x34\x7b\xbd\xc4\xec\x1d\x22\xe6\x74\x32\x63\x1e\xf2\x92\xcf\xe0\x1f\x0f\x61\x60\x98\x70\x68\x84\x36\x5f\x1c\x71\x6d\x0e\xf9\x9a\x9a\x99\xfd\x10\xb3\xbf\x44\x6b\xce\x9d\x23\x47\x5f\x1d\x72\x31\xbf\x4d\x79\xbf\xaf\xd3\x1a\xaf\x97\xb4\x0f\x0e\xb8\xa8\x35\xb9\x32\xc7\xe6\x93\xfb\xf8\x8b\xaa\x46\xd6\x99\x93\x4f\x5b\x01\x1a\x75\x5e\xb5\xc6\x6b\x2e\xe0\x99\x35\x5e\x53\x01\xcf\xd2\x68\xcb\xf5\x32\xdc\xdb\x6a\x64\xbd\x80\x38\x48\xf5\x7c\x94\x5d\xe7\x8f\xe0\xbd\xad\x4d\x77\x4f\xae\x2e\x37\x82\x03\xc8\x43\x6b\x7b\x7a\x88\xad\xf7\xb4\x2b\x09\x1f\x3d\x3c\xcc\x7b\x68\x12\x7e\xf6\xb0\x01\xfa\xc8\x66\x9f\xb7\xc9\xe0\x19\x22\x26\x8f\x8f\x54\x7b\x86\xc9\x51\x66\xd3\x87\xb2\xf1\x1a\x01\x5e\x66\xf5\xd6\xa3\x6b\xb4\x11\xea\x1f\xd7\xf5\x20\x68\x70\x8c\x91\x06\xe7\x04\x79\x34\x34\x43\x3e\x1f\x9a\x25\xbf\xf0\xbd\xe9\x7c\xe0\xa3\xce\x5f\x0f\xbf\xed\xfc\x64\xf8\x96\x13\xb8\xbf\x7c\x61\x03\x38\xf2\xd2\x9b\x87\x1f\xbf\x34\x8d\x60\xdd\xe3\x21\x9b\x63\x6f\x34\x1d\x40\xd7\x69\xed\xa5\x6b\xe4\xff\x98\xff\x31\x04\x9d\x97\xaf\xbb\x36\x27\x26\x9b\x0e\xa0\x1b\xb4\x76\xec\x12\xe9\x60\xfc\x09\x04\xc7\xc6\x5f\x77\x1d\x40\x01\x5a\x3b\x71\x83\x00\xfc\xf9\x8b\x2e\x80\x2e\x86\x0e\x80\x6e\x86\xc7\x01\x4e\x30\x3c\x09\x70\x92\xe1\x29\x80\x53\x64\x2a\xe0\x62\xcf\xbd\x00\xbd\x2c\xef\xa3\x00\x47\x19\xbe\x02\xd0\xc1\xf0\x18\xc0\xb1\x43\xe9\x43\xa9\x06\xa1\x21\x75\x48\xc4\x5b\x6f\x71\x6b\x5b\x2d\x57\x97\x70\xcb\x54\x1a\xb7\x4c\xaf\xe0\x16\xc5\x87\xdd\x57\x57\xb1\xf3\x6a\x06\xbb\xa7\xee\x61\xf7\xf4\x1a\x76\x4e\xad\x63\xe7\xf4\x06\x76\x2b\x09\xec\x54\xc2\xf8\xf4\xd5\x09\x7c\x7a\x6a\x12\x9f\x9e\xbe\x86\x4f\x2b\xd7\xf1\x89\xfe\x55\xf9\xc4\xd9\x7b\xf2\x89\xc1\x35\xf9\xc4\xf9\x84\xdc\xd5\x9f\x91\xbb\xce\xae\xcb\x5d\x83\x1b\x72\xd7\xf9\xb0\x7c\xaa\x7f\x42\x3e\x75\x76\x52\x3e\x35\x78\x4d\x3e\x75\xfe\xba\x7c\xa4\x7f\x49\x3e\x72\x36\x2d\x1f\x19\x5c\x91\x8f\x9c\xf7\xc9\xad\xa3\x4b\x72\xeb\xe5\xb4\xdc\x7a\x65\x45\x6e\x1d\xf7\xc9\xdd\xa3\xab\x72\xf7\xe5\x7b\x72\xf7\x95\x35\xb9\x7b\x3c\x21\xf7\x8e\x66\xe4\xde\xcb\xeb\x72\xef\x95\x0d\xb9\x77\x3c\x2c\x9f\x1c\x9d\x90\x4f\x5e\x9e\x94\x4f\x5e\xb9\x26\x9f\x1c\xbf\x2e\xb3\x3e\x68\xf3\x2f\xc9\x6d\x6a\x5a\x6e\x0b\xae\xc8\x6d\x21\x9f\xec\xf2\xaf\xca\x2e\xf5\x9e\xec\x0a\xae\xc9\xae\x50\x42\xf6\xf8\x33\xb2\x47\x5d\x97\x3d\xc1\x0d\xd9\x13\x0a\xcb\x3d\xfe\x09\xb9\x47\x9d\x94\x7b\x82\xd7\xe4\x9e\xd0\x75\xd9\x7a\x57\x46\x07\x97\xed\xa8\xee\xbd\x4e\x54\x7f\x81\xa0\x9a\x1f\x35\xa3\xfd\xef\xb7\xa0\xda\x0f\xda\xd0\x81\x85\x76\xb4\xef\x7e\x0d\xc2\x3f\xae\x45\x15\x3f\xb1\x22\x53\xa4\x0e\x09\x57\x25\x64\x98\xc2\x48\x9c\x36\x21\xa3\x52\x85\x62\x3b\xcb\xd9\xfe\xab\x4c\x55\xa6\x8c\x00\xc4\xe8\x70\x10\xa3\xd3\x49\x8c\x6e\x37\xa9\x34\x7f\x98\xed\xc5\x23\x3b\x2b\x5b\x66\x58\xda\x4a\x22\x89\x0a\x3c\xa6\x02\x6f\xa2\x02\x5f\x45\x8d\x7c\x1d\x35\xf2\xed\xd4\xc8\x13\x2a\xf2\x56\x6a\xe0\x6b\xa9\xc0\xd7\x50\x91\x6f\xa3\x06\xbe\x85\x0a\x7c\x33\x15\xf9\x4e\x6a\xe0\xed\x54\xe0\x65\xca\xea\xad\x10\xa1\x05\x60\xf9\xc9\x3e\x40\x80\x37\x2b\xd6\x79\x24\x52\x9c\x4e\x66\x80\x07\x52\x05\x40\x98\x4c\x00\x04\x1c\x32\x53\xc4\x01\xd9\x04\xd4\xc4\xb1\xbe\x62\x6f\x09\x84\x08\x6f\x10\x5c\x18\x44\x02\x88\xe9\xbe\xf3\x94\xb3\x02\x20\x0b\x72\xb1\x67\xa8\x01\xc0\x62\xb1\x0d\xf3\xbf\x6b\xa7\xd9\xec\x03\x60\x3a\x90\xd7\x11\x28\x02\x20\x88\xd7\xfc\x64\xff\x30\x9b\xda\xfa\xbe\xdf\x8b\x7b\xb4\x47\x7b\xb4\x47\x7b\xb4\x47\x7b\xf4\xdf\x4b\xf9\xb3\x66\xed\x9c\x58\x3f\x6e\xaf\xd3\xd0\xa0\xe1\xac\x26\xd7\x77\xfd\x16\x0d\xff\xf6\xcf\x9d\x18\xc3\x4e\x4d\xae\x9f\x2b\x8f\xa0\x67\xc7\xf3\x85\xa2\x37\xd5\xb8\xdb\xe6\xf3\x79\x6d\x0e\x7b\x27\xf8\xc3\x4a\x34\x68\xbb\x95\x3b\xd8\x64\x1c\xdb\xf3\xf3\xe5\xb4\xac\x5d\xfb\x8b\xf9\x46\x8d\xdf\x53\x5b\xcc\xaf\xd4\xf8\x49\x6b\x31\x7f\xbf\xc6\xff\xa4\x84\x5f\xaf\xf1\xff\x58\xc2\x7f\x85\xdd\x90\x71\xf7\x1c\x5f\xa3\x97\xca\x9c\x93\x83\x3d\xb0\x18\x9d\x5f\x8c\x80\x3d\x18\x5d\xb0\xcf\x2a\xf3\xb3\xa0\xdd\x19\x3f\x11\x07\x7b\x34\x96\x50\xc1\x9e\x50\x6f\x27\xb2\x3c\x25\x12\xf2\x83\xdd\x1f\x8b\x44\xd4\x68\x02\xec\xf3\x8b\x91\x84\x72\x03\xec\xf3\xb3\xf3\x89\x78\xee\x29\x87\xd0\xd7\xd7\x39\x4d\xb2\x77\x47\xf6\xde\x95\xbd\x3b\xb3\xf7\xee\xaf\x1c\xb3\x4f\x7b\xc7\x87\xcf\x5c\x38\xdf\xff\xfc\x59\x7d\x71\xe2\xb4\x1e\xc9\x2f\x2f\x2a\xc6\x4e\x6d\x28\x68\xba\xfa\xb9\xbd\xde\x4f\xfb\x00\xe0\xef\x3b\x3b\x31\xdd\x4c\xef\x1b\x1d\x6d\x05\x71\xa0\xc0\x5e\xa7\xfd\x5a\x4f\xea\xf6\x7a\x9f\xe9\x58\x57\x62\x2f\x94\x60\xbd\xf6\x0d\x81\x6e\xaf\xf7\xb5\x8e\x96\x67\xd4\x5b\x48\x2f\x6b\xbd\xa6\xdb\x97\xfb\xce\x02\x4a\xe2\xea\xd4\x5a\xf2\xed\x44\xb9\xef\x57\xca\x25\xd0\xa1\xd9\xf2\x3a\xa3\xcc\x77\x25\x7a\x5c\xa9\x24\x8c\x53\x73\xd9\x59\x12\x66\x4e\xb3\x6f\x2e\x13\x5e\xc7\xde\xc2\xb5\x2f\xa0\x07\x9a\xfd\xb6\x36\xae\xd0\xe6\xa9\x74\xfd\x07\x0b\x73\x2f\xa0\x37\xb4\xef\x8b\xc6\xcb\xc4\xd7\xe9\x62\x19\xfb\xdf\x68\xf6\xae\xaf\xb1\xff\x57\x00\x00\x00\xff\xff\x24\x63\xd3\x68\xf0\x24\x00\x00")

func kernelsHsacoBytes() ([]byte, error) {
	return bindataRead(
		_kernelsHsaco,
		"kernels.hsaco",
	)
}

func kernelsHsaco() (*asset, error) {
	bytes, err := kernelsHsacoBytes()
	if err != nil {
		return nil, err
	}

	info := bindataFileInfo{name: "kernels.hsaco", size: 9456, mode: os.FileMode(509), modTime: time.Unix(1541794681, 0)}
	a := &asset{bytes: bytes, info: info}
	return a, nil
}

// Asset loads and returns the asset for the given name.
// It returns an error if the asset could not be found or
// could not be loaded.
func Asset(name string) ([]byte, error) {
	cannonicalName := strings.Replace(name, "\\", "/", -1)
	if f, ok := _bindata[cannonicalName]; ok {
		a, err := f()
		if err != nil {
			return nil, fmt.Errorf("Asset %s can't read by error: %v", name, err)
		}
		return a.bytes, nil
	}
	return nil, fmt.Errorf("Asset %s not found", name)
}

// MustAsset is like Asset but panics when Asset would return an error.
// It simplifies safe initialization of global variables.
func MustAsset(name string) []byte {
	a, err := Asset(name)
	if err != nil {
		panic("asset: Asset(" + name + "): " + err.Error())
	}

	return a
}

// AssetInfo loads and returns the asset info for the given name.
// It returns an error if the asset could not be found or
// could not be loaded.
func AssetInfo(name string) (os.FileInfo, error) {
	cannonicalName := strings.Replace(name, "\\", "/", -1)
	if f, ok := _bindata[cannonicalName]; ok {
		a, err := f()
		if err != nil {
			return nil, fmt.Errorf("AssetInfo %s can't read by error: %v", name, err)
		}
		return a.info, nil
	}
	return nil, fmt.Errorf("AssetInfo %s not found", name)
}

// AssetNames returns the names of the assets.
func AssetNames() []string {
	names := make([]string, 0, len(_bindata))
	for name := range _bindata {
		names = append(names, name)
	}
	return names
}

// _bindata is a table, holding each asset generator, mapped to its name.
var _bindata = map[string]func() (*asset, error){
	"kernels.hsaco": kernelsHsaco,
}

// AssetDir returns the file names below a certain
// directory embedded in the file by go-bindata.
// For example if you run go-bindata on data/... and data contains the
// following hierarchy:
//     data/
//       foo.txt
//       img/
//         a.png
//         b.png
// then AssetDir("data") would return []string{"foo.txt", "img"}
// AssetDir("data/img") would return []string{"a.png", "b.png"}
// AssetDir("foo.txt") and AssetDir("notexist") would return an error
// AssetDir("") will return []string{"data"}.
func AssetDir(name string) ([]string, error) {
	node := _bintree
	if len(name) != 0 {
		cannonicalName := strings.Replace(name, "\\", "/", -1)
		pathList := strings.Split(cannonicalName, "/")
		for _, p := range pathList {
			node = node.Children[p]
			if node == nil {
				return nil, fmt.Errorf("Asset %s not found", name)
			}
		}
	}
	if node.Func != nil {
		return nil, fmt.Errorf("Asset %s not found", name)
	}
	rv := make([]string, 0, len(node.Children))
	for childName := range node.Children {
		rv = append(rv, childName)
	}
	return rv, nil
}

type bintree struct {
	Func     func() (*asset, error)
	Children map[string]*bintree
}

var _bintree = &bintree{nil, map[string]*bintree{
	"kernels.hsaco": &bintree{kernelsHsaco, map[string]*bintree{}},
}}

// RestoreAsset restores an asset under the given directory
func RestoreAsset(dir, name string) error {
	data, err := Asset(name)
	if err != nil {
		return err
	}
	info, err := AssetInfo(name)
	if err != nil {
		return err
	}
	err = os.MkdirAll(_filePath(dir, filepath.Dir(name)), os.FileMode(0755))
	if err != nil {
		return err
	}
	err = ioutil.WriteFile(_filePath(dir, name), data, info.Mode())
	if err != nil {
		return err
	}
	err = os.Chtimes(_filePath(dir, name), info.ModTime(), info.ModTime())
	if err != nil {
		return err
	}
	return nil
}

// RestoreAssets restores an asset under the given directory recursively
func RestoreAssets(dir, name string) error {
	children, err := AssetDir(name)
	// File
	if err != nil {
		return RestoreAsset(dir, name)
	}
	// Dir
	for _, child := range children {
		err = RestoreAssets(dir, filepath.Join(name, child))
		if err != nil {
			return err
		}
	}
	return nil
}

func _filePath(dir, name string) string {
	cannonicalName := strings.Replace(name, "\\", "/", -1)
	return filepath.Join(append([]string{dir}, strings.Split(cannonicalName, "/")...)...)
}
