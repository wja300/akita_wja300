// Package mineva implements mineva network
package mineva

import (
	"math"

	//"gitlab.com/akita/dnn/dataset/mnist"
	"gitlab.com/wja300/akita_wja300/dnn/dataset/mnist"
	//gitlab.com//dnn/dataset/mnist
	"gitlab.com/akita/dnn/layers"
	"gitlab.com/akita/dnn/training"
	"gitlab.com/akita/dnn/training/optimization"
	simLayers "gitlab.com/akita/mgpusim/benchmarks/dnn/layers"
	"gitlab.com/akita/mgpusim/driver"
)

type Benchmark struct {
	driver  *driver.Driver
	context *driver.Context
	gpus    []int

	network training.Network
	trainer training.Trainer
}

func NewBenchmark(driver *driver.Driver) *Benchmark {
	b := new(Benchmark)

	b.driver = driver
	b.context = b.driver.Init()

	b.network = training.Network{
		Layers: []layers.Layer{
			simLayers.CPUToGPULayer{
				GPUDriver: b.driver,
				GPUCtx:    b.context,
			},
			simLayers.NewFullyConnectedLayer(
				784, 256,
				b.driver, b.context,
				simLayers.NewMatrixOperator(b.driver, b.context),
			),
			simLayers.NewReluLayer(b.driver, b.context),
			simLayers.NewFullyConnectedLayer(
				256, 100,
				b.driver, b.context,
				simLayers.NewMatrixOperator(b.driver, b.context),
			),
			simLayers.NewReluLayer(b.driver, b.context),
			simLayers.NewFullyConnectedLayer(
				100, 100,
				b.driver, b.context,
				simLayers.NewMatrixOperator(b.driver, b.context),
			),
			simLayers.NewReluLayer(b.driver, b.context),
			simLayers.NewFullyConnectedLayer(
				100, 10,
				b.driver, b.context,
				simLayers.NewMatrixOperator(b.driver, b.context),
			),
			simLayers.GPUToCPULayer{
				GPUDriver: b.driver,
				GPUCtx:    b.context,
			},
		},
	}
	b.trainer = training.Trainer{
		DataSource: mnist.NewTrainingDataSource(),
		Network:    b.network,
		LossFunc:   training.SoftmaxCrossEntropy{},
		//OptimizationAlg: optimization.NewSGD(0.03),
		//OptimizationAlg: optimization.NewMomentum(0.1, 0.9),
		//OptimizationAlg: optimization.NewRMSProp(0.003),
		OptimizationAlg: optimization.NewAdam(0.001),
		Tester: &training.Tester{
			DataSource: mnist.NewTestDataSource(),
			Network:    b.network,
			BatchSize:  math.MaxInt32,
		},
		Epoch:         1000,
		BatchSize:     128,
		ShowBatchInfo: true,
	}

	return b
}

func (b *Benchmark) SelectGPU(gpuIDs []int) {
	if len(gpuIDs) > 1 {
		panic("multi-GPU is not supported by DNN workloads")
	}
}

func (b *Benchmark) Run() {
	for _, l := range b.network.Layers {
		l.Randomize()
	}
	b.trainer.Train()
}

func (b *Benchmark) Verify() {
	panic("not implemented")
}

func (b *Benchmark) SetUnifiedMemory() {
	panic("unified memory is not supported by dnn workloads")
}
