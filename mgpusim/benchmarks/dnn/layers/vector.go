package layers

import (
	"math"

	"gitlab.com/akita/dnn/tensor"
	"gitlab.com/akita/mgpusim/driver"
)

type Vector struct {
	size      int
	ptr       driver.GPUPtr
	GPUDriver *driver.Driver
	GPUCtx    *driver.Context
}

func (v Vector) AsMatrix(row, col int) *Matrix {
	m := &Matrix{
		col:  col,
		row:  row,
		data: v.ptr,
	}
	return m
}

func (v Vector) Raw() []float64 {
	tempData := make([]float32, v.size)
	v.GPUDriver.MemCopyD2H(v.GPUCtx, tempData, v.ptr)

	out := make([]float64, v.size)
	for i, value := range tempData {
		out[i] = float64(value)
	}

	return out
}

func (v Vector) Set(val []float64) {
	temp := make([]float32, v.size)
	for i, value := range temp {
		temp[i] = float32(value)
	}

	v.GPUDriver.MemCopyH2D(v.GPUCtx, v.ptr, temp)
}

func (v Vector) Clone() tensor.Vector {
	vector := &Vector{
		size:      v.size,
		GPUDriver: v.GPUDriver,
		GPUCtx:    v.GPUCtx,
	}
	vector.ptr = v.GPUDriver.AllocateMemory(v.GPUCtx, uint64(v.size*4))

	tempData := make([]float32, v.size)
	v.GPUDriver.MemCopyD2H(v.GPUCtx, tempData, v.ptr)
	v.GPUDriver.MemCopyH2D(v.GPUCtx, vector.ptr, tempData)

	return vector
}

func (v Vector) Scale(alpha float64) {
	raw := v.Raw()

	for i := range raw {
		raw[i] *= alpha
	}

	v.Set(raw)
}

func (v Vector) Add(b tensor.Vector) {
	aRaw := v.Raw()
	bRaw := b.Raw()

	for i := range aRaw {
		aRaw[i] += bRaw[i]
	}

	v.Set(aRaw)
}

func (v Vector) AddScalar(alpha float64) {
	aRaw := v.Raw()

	for i := range aRaw {
		aRaw[i] += alpha
	}

	v.Set(aRaw)
}

func (v Vector) ScaleAdd(alpha, beta float64, b tensor.Vector) {
	aRaw := v.Raw()
	bRaw := b.Raw()

	for i := range aRaw {
		aRaw[i] = alpha*aRaw[i] + beta*bRaw[i]
	}

	v.Set(aRaw)
}

func (v Vector) MulElemWise(b tensor.Vector) {
	aRaw := v.Raw()
	bRaw := b.Raw()

	for i := range aRaw {
		aRaw[i] *= bRaw[i]
	}

	v.Set(aRaw)
}

func (v Vector) DivElemWise(b tensor.Vector) {
	aRaw := v.Raw()
	bRaw := b.Raw()

	for i := range aRaw {
		aRaw[i] /= bRaw[i]
	}

	v.Set(aRaw)
}

func (v Vector) PowerScalar(alpha float64) {
	aRaw := v.Raw()

	for i := range aRaw {
		aRaw[i] = math.Pow(aRaw[i], alpha)
	}

	v.Set(aRaw)
}
