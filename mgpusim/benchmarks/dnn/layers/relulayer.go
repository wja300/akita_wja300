package layers

import (
	"gitlab.com/akita/dnn/tensor"
	"gitlab.com/akita/mgpusim/driver"
	"gitlab.com/akita/mgpusim/insts"
	"gitlab.com/akita/mgpusim/kernels"
)

type ReluLayer struct {
	GPUDriver *driver.Driver
	GPUCtx    *driver.Context

	forwardKernel  *insts.HsaCo
	backwardKernel *insts.HsaCo

	forwardInput driver.GPUPtr
}

func NewReluLayer(driver *driver.Driver, ctx *driver.Context) *ReluLayer {
	l := &ReluLayer{
		GPUDriver: driver,
		GPUCtx:    ctx,
	}

	kernelBytes := _escFSMustByte(false, "/relu.hsaco")
	l.forwardKernel = kernels.LoadProgramFromMemory(
		kernelBytes, "ReLUForward")
	if l.forwardKernel == nil {
		panic("fail to load relu forward kernel")
	}

	l.backwardKernel = kernels.LoadProgramFromMemory(
		kernelBytes, "ReLUBackward")
	if l.backwardKernel == nil {
		panic("fail to load relu backward kernel")
	}

	return l
}

func (l ReluLayer) Randomize() {
	// do nothing
}

type ForwardKernelArgs struct {
	Count, Padding int32
	In, Out        driver.GPUPtr
}

type BackwardKernelArgs struct {
	Count, Padding     int32
	ForwardIn, In, Out driver.GPUPtr
}

func (l *ReluLayer) Forward(inputT tensor.Tensor) tensor.Tensor {
	input := inputT.(*Tensor)
	size := input.Size()
	output := &Tensor{
		driver: l.GPUDriver,
		ctx:    l.GPUCtx,
		size:   size,
		ptr: l.GPUDriver.AllocateMemory(
			l.GPUCtx, uint64(size[0]*size[1]*4)),
	}

	l.saveInput(input)

	kernArg := ForwardKernelArgs{
		Count: int32(size[0] * size[1]),
		In:    input.ptr,
		Out:   output.ptr,
	}

	l.GPUDriver.LaunchKernel(l.GPUCtx, l.forwardKernel,
		[3]uint32{uint32(size[0] * size[1]), 1, 1},
		[3]uint16{64, 1, 1},
		&kernArg)

	return output
}

func (l *ReluLayer) saveInput(input *Tensor) {
	if l.forwardInput != 0 {
		l.GPUDriver.FreeMemory(l.GPUCtx, l.forwardInput)
	}

	numElement := input.Size()[0] * input.Size()[1]

	l.forwardInput = l.GPUDriver.AllocateMemory(l.GPUCtx,
		uint64(numElement*4))

	temp := make([]float32, numElement)
	l.GPUDriver.MemCopyD2H(l.GPUCtx, temp, input.ptr)
	l.GPUDriver.MemCopyH2D(l.GPUCtx, l.forwardInput, temp)
}

func (l *ReluLayer) Backward(inputT tensor.Tensor) tensor.Tensor {
	input := inputT.(*Tensor)
	size := input.Size()
	output := &Tensor{
		driver: l.GPUDriver,
		ctx:    l.GPUCtx,
		size:   size,
		ptr: l.GPUDriver.AllocateMemory(
			l.GPUCtx, uint64(size[0]*size[1]*4)),
	}

	kernArg := BackwardKernelArgs{
		Count:     int32(size[0] * size[1]),
		ForwardIn: l.forwardInput,
		In:        input.ptr,
		Out:       output.ptr,
	}

	l.GPUDriver.LaunchKernel(l.GPUCtx, l.backwardKernel,
		[3]uint32{uint32(size[0] * size[1]), 1, 1},
		[3]uint16{64, 1, 1},
		&kernArg)

	return output
}

func (l ReluLayer) Parameters() tensor.Vector {
	return nil
}

func (l ReluLayer) Gradients() tensor.Vector {
	return nil
}
