package main

import (
	"flag"

	//"gitlab.com/akita/mgpusim/benchmarks/dnn/mineva"
	"gitlab.com/wja300/akita_wja300/mgpusim/benchmarks/dnn/mineva"
	"gitlab.com/akita/mgpusim/samples/runner"
)

func main() {
	flag.Parse()

	runner := new(runner.Runner).ParseFlag().Init()

	benchmark := mineva.NewBenchmark(runner.GPUDriver)

	runner.AddBenchmark(benchmark)

	runner.Run()
}
